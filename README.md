# Everything is a File

## It's a UNIX System. I know this!

![Its a UNIX System, I know this](jurrasic-park-its-a-unix-system.jpg)

## Cheat Sheet 
https://docstore.mik.ua/manuals/hp-ux/en/5992-4580/ch03s02.html
man hier
https://man.openbsd.org/hier

# Introduction

There are multiple branches on the UNIX tree but they all have fundamemtal similarities. A system built around the time of Windows 95 will have the same basic structure as a system built today. There are minor differences between the various distributions and flavors of UNIX but they have many of the same components and often the same ideology behind their design. Simply, you'll always have a kernel, a shell, everything is a file, and most often a standardized set of commands accessible from the shell available in two major flavors the [GNU Coreutils](http://www.gnu.org/software/coreutils/manual/html_node/index.html) and Open Group [POSIX](https://pubs.opengroup.org/onlinepubs/9699919799/idx/utilities.html).


## Filesystem Hierarchy Standard

While not explicitly required, most UNIX systems follow the Filesystem Hierarchy Standard. This includes Linux, multiple BSD versions, and a reminder is available in most systems with the manual pages installed through the `$ man hier` command. 


## Logs

### Location

### Structure

## Files

### Text Files

### Compiled Programs

### Shell Scripts

### Interpreters 

## Processes

### Launch Process

### Memory Allocation

### Swap File

## Boot Process

### Kernel Loading

### Initialization

#### init.rc

#### systemd

### Runtime Control (RC) scripts
